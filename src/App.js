import React from 'react';
import './App.css';

import Guests from './guests/guests' 
import guests from './guests.json';
 
let searchEmptyLi = document.createElement("div");
    searchEmptyLi.innerHTML = "<h2>Ничего не найдено!</h2>"; 


class App extends React.Component{
  
  state = {
    search: '',
    guests
  } 

  showGuestList = ( searchInput ) => {  

    let arr = [];

    guests.filter(function(element){
      if( element.name.toLowerCase().indexOf(searchInput.toLowerCase()) >= 0  ||
          element.company.toLowerCase().indexOf(searchInput.toLowerCase()) >= 0 ||
          element.phone.toLowerCase().indexOf(searchInput.toLowerCase()) >= 0 ||
          element.address.toLowerCase().indexOf(searchInput.toLowerCase()) >= 0 ||
          element.about.toLowerCase().indexOf(searchInput.toLowerCase()) >= 0 
      ){   
          arr.push(element)
      } 
    })

    if(arr.length == 0){ 
      searchEmptyLi.hidden = false;  
      document.querySelector('.guest_list-list').append(searchEmptyLi)
    }

    return(
      arr.map( ({name, company, phone, address}, index ) =>  
                  <Guests
                    key = {index}
                    name = {name}
                    company = {company}
                    phone = {phone}
                    address = {address}
                  />
                )
    ) 
    
  }

  inputEvent = ( event ) => { 
    searchEmptyLi.hidden = true;  

    this.setState({
			search: event.target.value
    })
     
    this.showGuestList(event.target.value);
    
  }
  
  render = () =>{ 

    const { inputEvent, showGuestList } = this; 
    const { search } = this.state; 

      return (
         <>    
            <div className="guest_list-wrapper">
              <div className="guest_list-header">
                <div className="guest_list-header_top">
                    <h1>Список гостей</h1>
                    <span>Список жертв</span>
                    <img src="./search.png" alt="search" className="search_img"/>
                </div>
                <input onChange={inputEvent} value={search} type="search" name="search_guest" id="guest_search" placeholder="Введите имя гостя для поиска"/>
                 
              </div>
              <ul className="guest_list-list">     
                {      
                showGuestList( search )
                } 
              </ul>
            </div>
         </>
      );

  }

}

export default App;
