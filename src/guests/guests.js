import React, { Component } from 'react';
import './guests.css';


class Guests extends React.Component{

    state = {
        presence: false
    }

    changeHandler = () => {
        this.setState({
            presence: !this.state.presence
        })
    }

    render = () => {
        const   {name, company, phone, address} = this.props,
                {presence} = this.state;

        return(
            <>  
            <li className="guest_list-list_item" style={ presence ? { backgroundColor: "#94C11E" } : {  backgroundColor: "white" }}>
                <div className="list_item-desc">
                    <p>Гость <span className="text_bold">{name}</span> работает в компании <span className="text_bold">{company}</span></p>
                    <p>Его контакт: </p>
                    <p><span className="text_bold">{phone}</span></p>
                    <p><span className="text_bold">{address}</span></p>
                </div>
                <div className="list_item-button">
                    <input type="button" 
                    value="Прибыл"
                    onClick={this.changeHandler}
                    style={ presence ? { backgroundColor: "#EDEDED" } : {  backgroundColor: "#94C11E" }}
                    />
                </div>
            </li>
            </>
        );
    }
}

export default Guests;